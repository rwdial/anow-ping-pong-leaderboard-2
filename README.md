# Ping Pong Leaderboard Code Challenge

## Implementation

I have created the ping pong leaderboard with the following tools:

-   React
-   Webpack
-   Less

The project utilizes both the REST and websocket endpoints from the api for displaying the leaderboard.

This project can be run with:

```
yarn
yarn start
```

I have also added the Docker configuration, and while it does run locally for me I am not sure if I have implemented it correctly. I am not sure if the [api project]https://bitbucket.org/rwdial/anow-leaderboard-api/src which I have also forked is required.

```
docker-compose up
```

## Notes

Safari for Windows is no longer available to install, so I was not able to test with Safari.