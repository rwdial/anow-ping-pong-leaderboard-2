import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import Websocket from 'react-websocket';
import fetch from 'isomorphic-fetch';
import './app.less';
import logo from './anow.png';

require('es6-promise').polyfill();

function compareElo(a, b) {
  if (a.elo < b.elo) { return 1; }
  if (a.elo > b.elo) { return -1; }
  if (a.wins < b.wins) { return 1; }
  if (a.wins > b.wins) { return -1; }

  return 0;
}

function compareWL(a, b) {
  const aWL = a.losses > 0 ? a.wins / a.losses : Number.MAX_SAFE_INTEGER;
  const bWL = b.losses > 0 ? b.wins / b.losses : Number.MAX_SAFE_INTEGER;

  if (aWL < bWL) { return 1; }
  if (aWL > bWL) { return -1; }
  if (a.wins < b.wins) { return 1; }
  if (a.wins > b.wins) { return -1; }

  return 0;
}

function handleError() {
  // Do something with errors
}

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      sortMethod: 'elo',
      people: [],
      connected: true,
    };
  }

  componentWillMount() {
    fetch('http://localhost:3000/leaderboard')
      .then(response => response.json())
      .then((data) => {
        this.setState({ people: data });
        this.sortPeople(this.state.sortMethod);
      })
      .catch(e => handleError(e));
  }

  sortPeople(method) {
    if (method === 'elo') {
      this.setState({ people: this.state.people.sort(compareElo), sortMethod: method });
    } else {
      this.setState({ people: this.state.people.sort(compareWL), sortMethod: method });
    }
  }

  handleData(data) {
    this.setState({ people: JSON.parse(data) });
    this.sortPeople(this.state.sortMethod);
  }

  connectionOpened() {
    this.setState({ connected: true });
  }

  connectionClosed() {
    this.setState({ connected: false });
  }

  renderHeader() {
    return (
      <div className="header">
        <Websocket url="ws://localhost:3000/leaderboard" onOpen={() => this.connectionOpened()} onClose={() => this.connectionClosed()} onMessage={data => this.handleData(data)} />
        <img src={logo} alt="logo" />
        <h3>Ping Pong Ladder</h3>
        <div className="sortContainer">
          <span className="sortLabel">Sort By</span>
          <div className="btn-group" role="group" aria-label="...">
            <button type="button" className={`${this.state.sortMethod === 'elo' ? 'active' : ''} btn`} onClick={() => this.sortPeople('elo')}>ELO</button>
            <button type="button" className={`${this.state.sortMethod === 'winloss' ? 'active' : ''} btn`} onClick={() => this.sortPeople('winloss')}> Win/Loss</button>
          </div>
        </div>
      </div>
    );
  }

  renderTicker() {
    return (
      <div className="marquee">
        <span>
          { this.state.people.map((p, ind) => <span key={p.name} className="marqueeName">{ind + 1}. {p.name}</span>) }
        </span>
      </div>
    );
  }

  renderTable() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>Rank</th>
            <th>Name</th>
            <th className={`${this.state.sortMethod === 'elo' ? 'active' : ''} center`}>Elo</th>
            <th className="center">Wins</th>
            <th className="center">Losses</th>
            <th className={`${this.state.sortMethod === 'winloss' ? 'active' : ''} center`}>W/L</th>
          </tr>
        </thead>
        <tbody>
          { this.state.people.map((p, ind) =>
            <Person key={p.name} pos={ind} {...p} sort={this.state.sortMethod} />)
          }
          { this.state.people.length === 0 && <tr><td className="center italic" colSpan="6">No people retrieved</td></tr> }
        </tbody>
      </table>);
  }

  render() {
    return (
      <div className="container-fluid">
        { !this.state.connected && <div className="alert alert-danger">The connection to the leaderboard service is unavailable - attempting to reconnect</div> }
        <div className="panel table-responsive">
          { this.renderHeader() }
          { this.renderTicker() }
          { this.renderTable() }
        </div>
      </div>);
  }
}

export default App;

const Person = ({
  name, wins, losses, elo, pos, sort,
}) => (
  <tr key={name}>
    <td>{pos + 1}</td>
    <td>{name}</td>
    <td className={`${sort === 'elo' ? 'active' : ''} center`}>{elo}</td>
    <td className="center">{wins}</td>
    <td className="center">{losses}</td>
    <td className={`${sort === 'winloss' ? 'active' : ''} center`}>{(losses > 0 ? (wins / losses).toFixed(2) : '-')}</td>
  </tr>);
Person.propTypes = {
  name: PropTypes.string.isRequired,
  wins: PropTypes.number.isRequired,
  losses: PropTypes.number.isRequired,
  elo: PropTypes.number.isRequired,
  pos: PropTypes.number.isRequired,
  sort: PropTypes.string.isRequired,
};

ReactDOM.render(<App />, document.getElementById('main'));
