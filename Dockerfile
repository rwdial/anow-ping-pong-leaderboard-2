FROM node:8.9-alpine
ENV NODE_ENV production
WORKDIR /usr/src/app
COPY ["package.json", "yarn.lock*", "webpack.config.json*", "./"]
RUN yarn && mv node_modules ../
COPY . .
EXPOSE 3001
CMD node server.js