const express = require('express');
const path = require('path');

const app = express();

app.use('/images', express.static(path.join(__dirname, './images')));
app.use('/', express.static(path.join(__dirname, './bin')));

app.listen(process.env.PORT || 3001);
